import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'document_model.g.dart';

@JsonSerializable()
class DocumentModel extends Equatable {
  static const String langRu = 'ru';
  static const String langKg = 'kg';
  static const String langEn = 'en';

  final String path;
  final String lang;
  @JsonKey(name: 'account_number')
  final String accountNumber;
  @JsonKey(name: 'doc_type')
  final String docType;
  final DocCoordinates coordinates;

  DocumentModel(
      {this.path,
      this.lang,
      this.accountNumber,
      this.docType,
      this.coordinates});

  factory DocumentModel.fromJson(Map<String, dynamic> json) =>
      _$DocumentModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocumentModelToJson(this);

  @override
  List<Object> get props => [path, lang, accountNumber, docType, coordinates];

  @override
  bool get stringify => true;

  DocumentModel copyWith({
    String path,
    String lang,
    String accountNumber,
    String docType,
    DocCoordinates coordinates,
  }) {
    return DocumentModel(
      path: path ?? this.path,
      lang: lang ?? this.lang,
      accountNumber: accountNumber ?? this.accountNumber,
      docType: docType ?? this.docType,
      coordinates: coordinates ?? this.coordinates,
    );
  }
}

@JsonSerializable()
class DocCoordinates extends Equatable {
  final int x;
  final int y;
  final int width;
  final int height;

  DocCoordinates({this.x, this.y, this.width, this.height});

  factory DocCoordinates.fromJson(Map<String, dynamic> json) =>
      _$DocCoordinatesFromJson(json);
  Map<String, dynamic> toJson() => _$DocCoordinatesToJson(this);

  @override
  List<Object> get props => [x, y, width, height];

  @override
  bool get stringify => true;
}
