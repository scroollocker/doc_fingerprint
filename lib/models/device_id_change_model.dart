import 'package:json_annotation/json_annotation.dart';

part 'device_id_change_model.g.dart';

@JsonSerializable()
class DeviceIdChangeModel {
  final bool status;
  @JsonKey(name: 'device_id')
  final String deviceId;
  final String message;

  DeviceIdChangeModel({this.status, this.deviceId, this.message});

  factory DeviceIdChangeModel.fromJson(Map<String, dynamic> json) =>
      _$DeviceIdChangeModelFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceIdChangeModelToJson(this);
}

@JsonSerializable()
class DeviceIdPayload {
  @JsonKey(name: 'device_id')
  final String deviceId;

  DeviceIdPayload({this.deviceId});

  factory DeviceIdPayload.fromJson(Map<String, dynamic> json) =>
      _$DeviceIdPayloadFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceIdPayloadToJson(this);
}
