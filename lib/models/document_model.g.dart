// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DocumentModel _$DocumentModelFromJson(Map<String, dynamic> json) {
  return DocumentModel(
    path: json['path'] as String,
    lang: json['lang'] as String,
    accountNumber: json['account_number'] as String,
    docType: json['doc_type'] as String,
    coordinates: json['coordinates'] == null
        ? null
        : DocCoordinates.fromJson(json['coordinates'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DocumentModelToJson(DocumentModel instance) =>
    <String, dynamic>{
      'path': instance.path,
      'lang': instance.lang,
      'account_number': instance.accountNumber,
      'doc_type': instance.docType,
      'coordinates': instance.coordinates,
    };

DocCoordinates _$DocCoordinatesFromJson(Map<String, dynamic> json) {
  return DocCoordinates(
    x: json['x'] as int,
    y: json['y'] as int,
    width: json['width'] as int,
    height: json['height'] as int,
  );
}

Map<String, dynamic> _$DocCoordinatesToJson(DocCoordinates instance) =>
    <String, dynamic>{
      'x': instance.x,
      'y': instance.y,
      'width': instance.width,
      'height': instance.height,
    };
