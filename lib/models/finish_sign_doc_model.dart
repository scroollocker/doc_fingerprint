import 'package:json_annotation/json_annotation.dart';

part 'finish_sign_doc_model.g.dart';

@JsonSerializable()
class FinishSignDocModel {
  @JsonKey(name: 'device_id')
  final String deviceId;
  @JsonKey(name: 'account_number')
  final String accountNumber;
  @JsonKey(name: 'signed_file')
  final String fileId;
  @JsonKey(name: 'doc_type')
  final String docType;

  FinishSignDocModel({
    this.deviceId,
    this.accountNumber,
    this.fileId,
    this.docType,
  });

  factory FinishSignDocModel.fromJson(Map<String, dynamic> json) =>
      _$FinishSignDocModelFromJson(json);
  Map<String, dynamic> toJson() => _$FinishSignDocModelToJson(this);

  @override
  String toString() {
    return 'FinishSignDocModel(deviceId: $deviceId, accountNumber: $accountNumber, fileId: $fileId, fileType: $docType)';
  }
}
