// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_id_change_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceIdChangeModel _$DeviceIdChangeModelFromJson(Map<String, dynamic> json) {
  return DeviceIdChangeModel(
    status: json['status'] as bool,
    deviceId: json['device_id'] as String,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$DeviceIdChangeModelToJson(
        DeviceIdChangeModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'device_id': instance.deviceId,
      'message': instance.message,
    };

DeviceIdPayload _$DeviceIdPayloadFromJson(Map<String, dynamic> json) {
  return DeviceIdPayload(
    deviceId: json['device_id'] as String,
  );
}

Map<String, dynamic> _$DeviceIdPayloadToJson(DeviceIdPayload instance) =>
    <String, dynamic>{
      'device_id': instance.deviceId,
    };
