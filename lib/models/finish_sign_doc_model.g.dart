// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'finish_sign_doc_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FinishSignDocModel _$FinishSignDocModelFromJson(Map<String, dynamic> json) {
  return FinishSignDocModel(
    deviceId: json['device_id'] as String,
    accountNumber: json['account_number'] as String,
    fileId: json['signed_file'] as String,
    docType: json['doc_type'] as String,
  );
}

Map<String, dynamic> _$FinishSignDocModelToJson(FinishSignDocModel instance) =>
    <String, dynamic>{
      'device_id': instance.deviceId,
      'account_number': instance.accountNumber,
      'signed_file': instance.fileId,
      'doc_type': instance.docType,
    };
