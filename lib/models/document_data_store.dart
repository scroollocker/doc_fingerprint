import 'package:doc_fingerprint/models/document_model.dart';

class DocumentDataStore {
  String deviceId;
  DocumentModel document;
  DocumentModel signedDocument;

  DocumentDataStore({this.deviceId, this.document, this.signedDocument});

  DocumentDataStore copyWith({
    String deviceId,
    DocumentModel document,
    DocumentModel signedDocument,
  }) {
    return DocumentDataStore(
      deviceId: deviceId ?? this.deviceId,
      document: document ?? this.document,
      signedDocument: signedDocument ?? this.signedDocument,
    );
  }

  @override
  String toString() =>
      'DocumentDataStore(deviceId: $deviceId, document: $document, signedDocument: $signedDocument)';
}
