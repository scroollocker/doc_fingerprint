import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:doc_fingerprint/constants/socket_constants.dart';
import 'package:doc_fingerprint/models/device_id_change_model.dart';
import 'package:doc_fingerprint/models/document_model.dart';
import 'package:doc_fingerprint/models/finish_sign_doc_model.dart';
import 'package:doc_fingerprint/service/pdf_helper.dart';
import 'package:doc_fingerprint/service/settings_helper.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_pdf_box/flutter_pdf_box.dart';
import 'package:get_it/get_it.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

part 'socket_event.dart';
part 'socket_state.dart';

typedef AddDispatch = void Function(SocketEvent);

class SocketBloc extends Bloc<SocketEvent, SocketState> {
  IO.Socket _socket;

  SocketBloc(this._socket) : super(SocketInitial());

  Future<void> _onIdChange(dynamic data, AddDispatch add) async {
    var response = DeviceIdChangeModel.fromJson(data);
    if (response.status == true) {
      add(SaveDeviceId(response.deviceId));
    } else {
      add(SocketErrorEvent(message: response.message));
    }
  }

  Future<void> _onDocumentFetched(dynamic data, AddDispatch add) async {
    var response = DocumentModel.fromJson(data);
    add(DocumentIsLoadingEvent(response));
  }

  Future<void> _onSocketConnected(_, AddDispatch add) async {
    add(ConnectToSocketComplete());
  }

  Future<void> _onDeviceSetAwait(_, AddDispatch add) async {
    add(CancelByOperator());
  }

  Future<void> _onReconnecting(_, AddDispatch add) async {
    add(ReconnectingEvent());
  }

  @override
  Stream<SocketState> mapEventToState(
    SocketEvent event,
  ) async* {
    if (event is InitSocketEvent) {
      // System callbacks
      _socket.onConnect((_) {
        print('Connected');
        _onSocketConnected(_, add);
      });
      _socket.onDisconnect((_) => print('Disconnected'));
      _socket.onReconnecting((_) {
        print('Reconnecting');
        _onReconnecting(_, add);
      });
      _socket.onReconnectAttempt((_) => print('Recconnect Attemt'));

      //On id change result
      _socket.on(SocketEventConstants.onDeviceIdChanged,
          (data) => _onIdChange(data, add));

      //On document fetched
      _socket.on(SocketEventConstants.onPrepareSignDoc,
          (data) => _onDocumentFetched(data, add));

      _socket.on(SocketEventConstants.onDeviceSetAwait,
          (data) => _onDeviceSetAwait(data, add));

      await FlutterPdfBox.init();

      yield OnInitComplete();
    }
    if (event is ConnectWithDeviceId) {
      yield OnLoadingScreen();
      _socket.emit(SocketActionConstants.deviceConnected,
          DeviceIdPayload(deviceId: event.deviceId).toJson());
      yield OnWaitDocument();
    }
    if (event is ReConnectWithDeviceId) {
      _socket.emit(SocketActionConstants.deviceConnected,
          DeviceIdPayload(deviceId: event.deviceId).toJson());
    }
    if (event is LoadDeviceIdFromSettings) {
      String deviceId = await GetIt.I<SettingsHelper>().getDeviceId();

      yield OnDeviceIdLoaded(deviceId);
    }

    if (event is ChangeDeviceId) {
      yield LoadingState();
      _socket.emit(SocketActionConstants.deviceIdChanged,
          DeviceIdPayload(deviceId: event.deviceId).toJson());
    }

    if (event is SaveDeviceId) {
      await GetIt.I<SettingsHelper>().saveDeviceId(event.deviceId);
      yield OnIdChange(event.deviceId);
    }

    if (event is DocumentIsLoadingEvent) {
      yield OnDocumentLoaded(event.document);
    }

    if (event is SendSignInProgress) {
      String deviceId = await GetIt.I<SettingsHelper>().getDeviceId();
      _socket.emit(SocketActionConstants.signDocInProgress,
          DeviceIdPayload(deviceId: deviceId).toJson());
    }

    if (event is SendSignCancel) {
      String deviceId = await GetIt.I<SettingsHelper>().getDeviceId();
      _socket.emit(SocketActionConstants.signCancel,
          DeviceIdPayload(deviceId: deviceId).toJson());
      yield OnWaitDocument();
    }
    if (event is ConnectToSocketComplete) {
      yield OnSocketConnected();
    }
    if (event is CancelByOperator) {
      yield OnWaitDocument();
    }
    if (event is SignPdf) {
      var page = PDFPage.modify(0);
      page.drawImage(
          imagePath: event.pdfSignPath,
          imageSource: PDFActions.PDFActionImageSourcePath,
          imageType: PDFActions.PDFActionImageTypePng,
          height: event.documentModel.coordinates.height,
          width: event.documentModel.coordinates.width,
          x: event.documentModel.coordinates.x,
          y: event.documentModel.coordinates.y);

      var doc = PDFDocument.modify(event.pdfFilePath);
      doc = doc.modifyPage(page);
      await doc.write();

      // await Future.delayed(Duration(seconds: 5));

      // yield OnWaitDocument();
      yield OnDocumentSignConfirmed(
          event.documentModel.copyWith(path: event.pdfFilePath));
    }

    if (event is UploadPdfToServer) {
      try {
        yield UploadingState();
        String deviceId = await GetIt.I<SettingsHelper>().getDeviceId();
        var fileId = await PdfHelper.uploadPdf(path: event.path);
        _socket.emit(
            SocketActionConstants.finishSignDoc,
            FinishSignDocModel(
                    accountNumber: event.document.accountNumber,
                    deviceId: deviceId,
                    fileId: fileId,
                    docType: event.document.docType)
                .toJson());
        yield OnWaitDocument();
      } catch (err) {
        print(err);
      }
    }
    if (event is SocketErrorEvent) {
      yield OnSocketError(error: event.message);
    }
    if (event is ReconnectingEvent) {
      yield ReconnectingState();
    }
  }
}
