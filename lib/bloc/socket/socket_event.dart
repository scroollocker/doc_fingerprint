part of 'socket_bloc.dart';

abstract class SocketEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class InitSocketEvent extends SocketEvent {}

class ConnectToSocketComplete extends SocketEvent {}

class ConnectWithDeviceId extends SocketEvent {
  final String deviceId;
  ConnectWithDeviceId(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];

  @override
  bool get stringify => true;
}

class ReConnectWithDeviceId extends SocketEvent {
  final String deviceId;
  ReConnectWithDeviceId(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];

  @override
  bool get stringify => true;
}

class ChangeDeviceId extends SocketEvent {
  final String deviceId;
  ChangeDeviceId(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];

  @override
  bool get stringify => true;
}

class SaveDeviceId extends SocketEvent {
  final String deviceId;
  SaveDeviceId(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];

  @override
  bool get stringify => true;
}

class ReconnectingEvent extends SocketEvent {}

class LoadDeviceIdFromSettings extends SocketEvent {}

class DocumentIsLoadingEvent extends SocketEvent {
  final DocumentModel document;

  DocumentIsLoadingEvent(this.document);

  @override
  List<Object> get props => [document];

  @override
  bool get stringify => true;
}

class SendSignInProgress extends SocketEvent {}

class SendSignCancel extends SocketEvent {}

class CancelByOperator extends SocketEvent {}

class SignPdf extends SocketEvent {
  final String pdfFilePath;
  final String pdfSignPath;
  final DocumentModel documentModel;

  SignPdf({this.pdfFilePath, this.pdfSignPath, this.documentModel});

  @override
  List<Object> get props => [pdfFilePath, pdfSignPath, documentModel];

  @override
  bool get stringify => true;
}

class UploadPdfToServer extends SocketEvent {
  final String path;
  final DocumentModel document;

  UploadPdfToServer({this.path, this.document});

  @override
  List<Object> get props => [path, document];

  @override
  bool get stringify => true;
}

class SocketErrorEvent extends SocketEvent {
  final String message;

  SocketErrorEvent({this.message});

  @override
  List<Object> get props => [message];

  @override
  bool get stringify => true;
}
