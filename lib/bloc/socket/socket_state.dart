part of 'socket_bloc.dart';

abstract class SocketState extends Equatable {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class SocketInitial extends SocketState {}

class OnInitComplete extends SocketState {}

class OnSocketConnected extends SocketState {}

class OnIdChange extends SocketState {
  final String deviceId;
  OnIdChange(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];
}

class OnLoadingScreen extends SocketState {}

class OnWaitDocument extends SocketState {}

class OnDeviceIdLoaded extends SocketState {
  final String deviceId;
  OnDeviceIdLoaded(
    this.deviceId,
  );

  @override
  List<Object> get props => [deviceId];
}

class OnDocumentLoaded extends SocketState {
  final DocumentModel document;

  OnDocumentLoaded(this.document);

  @override
  List<Object> get props => [document];
}

class OnDocumentSignConfirmed extends SocketState {
  final DocumentModel document;

  OnDocumentSignConfirmed(this.document);

  @override
  List<Object> get props => [document];
}

class OnSocketError extends SocketState {
  final String error;
  OnSocketError({this.error});

  @override
  List<Object> get props => [error];
}

class LoadingState extends SocketState {}

class UploadingState extends SocketState {}

class ReconnectingState extends SocketState {}
