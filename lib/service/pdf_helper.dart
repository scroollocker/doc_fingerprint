import 'package:dio/dio.dart';
import 'package:doc_fingerprint/constants/settings_constants.dart';
import 'package:http_parser/http_parser.dart';

class PdfHelper {
  static Future<String> downloadPdf({String path, String url}) {
    assert(path != null && url != null);

    var dio = Dio();

    return dio.download(url, path).then((r) {
      if (r.statusCode == 200) {
        return path;
      } else {
        throw Exception('Ошибка загрузки документа');
      }
    }).catchError((err) {
      print(err);
      throw Exception('Ошибка загрузки документа');
    });
  }

  static Future<String> uploadPdf({String path}) async {
    assert(path != null);

    var dio = Dio();

    var formData = FormData.fromMap({
      'pdf': await MultipartFile.fromFile(path,
          filename: 'doc.pdf', contentType: MediaType('application', 'pdf'))
    });

    var url = '${SettingsConstants.apiUrlAddress}upload_pdf';
    try {
      var response = await dio.post(url, data: formData);
      if (response.statusCode == 200) {
        return response.data['file_id'];
      } else {
        return null;
      }
    } catch (err) {
      print(err);
      throw Exception('Ошибка загрузки файла');
    }
  }
}

//finish_sign_doc - {"doc_type":"D","signed_file":"2fc1423d571d5004664b3fe3f53f77be","device_id":"73","account_number":"21745070"}
//finish_sign_doc - {"device_id":"295","account_number":"16524723","file_id":"b779c4aa00bb22924b53c0ae3416908a","doc_type":"Z-SIM"}
