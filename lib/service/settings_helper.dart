import 'package:doc_fingerprint/constants/settings_constants.dart';
import 'package:hive/hive.dart';

class SettingsHelper {
  Future<void> saveDeviceId(String deviceId) async {
    assert(deviceId != null);
    var box = Hive.isBoxOpen(SettingsConstants.settingsBoxName)
        ? Hive.box(SettingsConstants.settingsBoxName)
        : await Hive.openBox(SettingsConstants.settingsBoxName);
    await box.put(SettingsConstants.settingsDeviceIdKey, deviceId);
  }

  Future<String> getDeviceId() async {
    var box = Hive.isBoxOpen(SettingsConstants.settingsBoxName)
        ? Hive.box(SettingsConstants.settingsBoxName)
        : await Hive.openBox(SettingsConstants.settingsBoxName);
    return box.get(SettingsConstants.settingsDeviceIdKey);
  }
}
