import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';

abstract class _DrawablePaintAreaBase {
  void clear();
  Future<String> save();
}

class DrawablePaintArea extends StatefulWidget {
  final double width;
  final double height;
  final double strokeWidth;
  final Color strokeColor;
  final SignAreaController controller;
  final String lang;

  const DrawablePaintArea(
      {Key key,
      this.width = 150,
      this.height = 100,
      this.strokeWidth = 3.0,
      this.strokeColor = Colors.black,
      this.controller,
      this.lang = 'ru'})
      : super(key: key);

  @override
  _DrawablePaintAreaState createState() => _DrawablePaintAreaState();
}

class _DrawablePaintAreaState extends State<DrawablePaintArea>
    implements _DrawablePaintAreaBase {
  GlobalKey _repaintBoundaryKey = GlobalKey();
  List<Offset> _points = List();

  Paint _strokePaint;

  @override
  void initState() {
    super.initState();
    widget.controller?.setView(this);
    _strokePaint = Paint()
      ..color = widget.strokeColor
      ..isAntiAlias = true
      ..strokeCap = StrokeCap.round
      ..strokeWidth = widget.strokeWidth;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
                image: AssetImage('assets/imgs/sign_back_${widget.lang}.png'),
                fit: BoxFit.fill)),
        // color: Colors.red,
        child: GestureDetector(
          onPanUpdate: (details) {
            print(details.localPosition);
            if (details.localPosition.dx < 0 ||
                details.localPosition.dy < 0 ||
                details.localPosition.dx > widget.width ||
                details.localPosition.dy > widget.height) return;
            setState(() {
              _points.add(details.localPosition);
            });
          },
          onPanStart: (details) {
            if (details.localPosition.dx < 0 || details.localPosition.dy < 0)
              return;
            setState(() {
              _points.add(details.localPosition);
            });
          },
          onPanEnd: (_) {
            setState(() {
              _points.add(null);
            });
          },
          child: RepaintBoundary(
            key: _repaintBoundaryKey,
            child: CustomPaint(
              size: Size(widget.width, widget.height),
              painter: CanvasDrawer(points: _points, paintObj: _strokePaint),
            ),
          ),
        ));
  }

  @override
  void clear() {
    print('Clear called');
    setState(() => _points.clear());
  }

  @override
  Future<String> save() async {
    print('Save called');
    RenderRepaintBoundary boundary =
        _repaintBoundaryKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();

    var _tmpDir = await getTemporaryDirectory();
    var _signImgPath =
        '${_tmpDir.path}/${DateTime.now().microsecondsSinceEpoch}_sign.png';
    File _imgFile = File(_signImgPath);
    await _imgFile.writeAsBytes(pngBytes);
    return _signImgPath;
  }
}

class CanvasDrawer extends CustomPainter {
  List<Offset> points;
  List<Offset> offsets = List();
  final Paint paintObj;

  CanvasDrawer({this.points, this.paintObj});

  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        //Drawing line when two consecutive points are available
        canvas.drawLine(points[i], points[i + 1], paintObj);
      } else if (points[i] != null && points[i + 1] == null) {
        offsets.clear();
        offsets.add(points[i]);
        offsets.add(Offset(points[i].dx + 0.1, points[i].dy + 0.1));

        //Draw points when two points are not next to each other
        canvas.drawPoints(ui.PointMode.points, offsets, paintObj);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

class SignAreaController implements _DrawablePaintAreaBase {
  _DrawablePaintAreaBase _view;

  void setView(_DrawablePaintAreaBase view) {
    print('Set view called');
    _view = view;
  }

  @override
  void clear() => _view?.clear();

  @override
  Future<String> save() => _view?.save();
}
