import 'package:doc_fingerprint/constants/lang.dart';
import 'package:doc_fingerprint/models/document_model.dart';
import 'package:flutter/material.dart';

import 'package:doc_fingerprint/ui/components/drawable_paint_area.dart';
import 'package:doc_fingerprint/ui/components/ui_button.dart';
import 'package:doc_fingerprint/ui/components/ui_buttons_row.dart';

class UISignArea extends StatefulWidget {
  final DocumentModel documentModel;

  const UISignArea({Key key, @required this.documentModel}) : super(key: key);

  @override
  _UISignAreaState createState() => _UISignAreaState();
}

class _UISignAreaState extends State<UISignArea> {
  SignAreaController _areaController;

  @override
  void initState() {
    super.initState();
    _areaController = SignAreaController();
  }

  @override
  Widget build(BuildContext context) {
    var langCode = widget.documentModel.lang;

    return Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DrawablePaintArea(
              height: 239,
              width: 667,
              controller: _areaController,
              lang: langCode,
            ),
            UIButtonsRow(
              buttons: [
                UIButton(
                    text: Lang.tr(langCode, Lang.sign_confirm),
                    color: Colors.green,
                    textColor: Colors.white,
                    onTap: () async {
                      String path = await _areaController.save();
                      Navigator.pop(context, path);
                    }),
                UIButton(
                    text: Lang.tr(langCode, Lang.sign_clear),
                    color: Colors.blue,
                    textColor: Colors.white,
                    onTap: () => _areaController.clear()),
                UIButton(
                    text: Lang.tr(langCode, Lang.sign_cancel),
                    color: Colors.red,
                    textColor: Colors.white,
                    onTap: () => Navigator.pop(context)),
              ],
            ),
            SizedBox(height: 32)
          ],
        ));
  }
}
