import 'package:flutter/material.dart';

class UIButton extends StatelessWidget {
  final VoidCallback onTap;
  final String text;
  final Color color;
  final Color textColor;

  const UIButton(
      {Key key,
      this.onTap,
      @required this.text,
      this.color = Colors.blue,
      this.textColor = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 18),
        onPressed: onTap,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.zero),
        color: color,
        textColor: textColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text('$text', style: TextStyle(fontSize: 18)),
        ));
  }
}
