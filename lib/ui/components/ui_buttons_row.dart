import 'package:flutter/material.dart';

import 'package:doc_fingerprint/ui/components/ui_button.dart';

class UIButtonsRow extends StatelessWidget {
  final List<UIButton> buttons;
  const UIButtonsRow({
    Key key,
    @required this.buttons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var buttonsWrapedWidget =
        buttons?.map<Widget>((e) => Expanded(child: e))?.toList();
    return Container(child: Row(children: buttonsWrapedWidget));
  }
}
