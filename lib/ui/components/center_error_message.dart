import 'package:flutter/material.dart';

class CenterErrorMessage extends StatelessWidget {
  final String errorText;

  CenterErrorMessage({
    Key key,
    this.errorText,
  }) : super(key: key) {
    assert(errorText != null);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Container(
            child: Text(
              '$errorText',
              style: TextStyle(fontSize: 22, color: Colors.black54),
            ),
          ),
        ),
      ],
    );
  }
}
