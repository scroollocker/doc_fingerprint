import 'package:doc_fingerprint/bloc/socket/socket_bloc.dart';
import 'package:doc_fingerprint/constants/lang.dart';
import 'package:doc_fingerprint/models/document_data_store.dart';
import 'package:doc_fingerprint/models/document_model.dart';
import 'package:doc_fingerprint/ui/components/ui_button.dart';
import 'package:doc_fingerprint/ui/components/ui_buttons_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class SignConfirmButtons extends StatelessWidget {
  final DocumentModel documentModel;

  SignConfirmButtons({@required this.documentModel});

  @override
  Widget build(BuildContext context) {
    var langCode = documentModel.lang;

    return UIButtonsRow(
      buttons: [
        UIButton(
            text: Lang.tr(langCode, Lang.confirm_sign),
            color: Colors.green,
            textColor: Colors.white,
            onTap: () {
              var document = GetIt.I<DocumentDataStore>().signedDocument;
              // Upload pdf and send to Backend
              BlocProvider.of<SocketBloc>(context).add(
                  UploadPdfToServer(document: document, path: document.path));
            }),
        UIButton(
            text: Lang.tr(langCode, Lang.resign_btd),
            color: Colors.red,
            textColor: Colors.white,
            onTap: () {
              // Reload start document without sign
              var document = GetIt.I<DocumentDataStore>().document;
              BlocProvider.of<SocketBloc>(context)
                  .add(DocumentIsLoadingEvent(document));
            })
      ],
    );
  }
}
