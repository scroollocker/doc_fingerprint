import 'package:doc_fingerprint/bloc/socket/socket_bloc.dart';
import 'package:doc_fingerprint/constants/lang.dart';
import 'package:doc_fingerprint/models/document_model.dart';
import 'package:doc_fingerprint/ui/components/ui_button.dart';
import 'package:doc_fingerprint/ui/components/ui_buttons_row.dart';
import 'package:doc_fingerprint/ui/components/ui_sign_area.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignStartButtons extends StatelessWidget {
  final DocumentModel documentModel;
  final String pdfPath;
  final Function(bool showed) onChangeShowState;

  const SignStartButtons(
      {Key key,
      @required this.documentModel,
      @required this.pdfPath,
      this.onChangeShowState})
      : super(key: key);

  void _onChangeShowState(bool showed) {
    if (onChangeShowState != null) {
      onChangeShowState(showed);
    }
  }

  @override
  Widget build(BuildContext context) {
    var langCode = documentModel.lang;
    return UIButtonsRow(
      buttons: [
        UIButton(
            text: Lang.tr(langCode, Lang.sign_document_btn),
            color: Colors.blue,
            textColor: Colors.white,
            onTap: () async {
              _onChangeShowState(true);

              BlocProvider.of<SocketBloc>(context).add(SendSignInProgress());
              print('Doc start signing');
              var result = await showModalBottomSheet<String>(
                  context: context,
                  enableDrag: false,
                  isDismissible: false,
                  builder: (context) {
                    return UISignArea(
                      documentModel: documentModel,
                    );
                  });
              print('Dialog was been closed');
              print(result);
              print(pdfPath);
              if (result != null) {
                BlocProvider.of<SocketBloc>(context).add(SignPdf(
                    documentModel: documentModel,
                    pdfFilePath: pdfPath,
                    pdfSignPath: result));
              }
              _onChangeShowState(false);
            }),
        UIButton(
            text: Lang.tr(langCode, Lang.cancel_sign_doc),
            color: Colors.red,
            textColor: Colors.white,
            onTap: () {
              _onChangeShowState(false);
              BlocProvider.of<SocketBloc>(context).add(SendSignCancel());
            })
      ],
    );
  }
}
