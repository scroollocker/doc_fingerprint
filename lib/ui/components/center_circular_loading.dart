import 'package:flutter/material.dart';

class CenterCircularLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [Center(child: CircularProgressIndicator())],
    );
  }
}
