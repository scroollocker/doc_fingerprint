import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoadingOverlay {
  static OverlayEntry _instanceOfEntry;

  static void showLoadingOverlay(BuildContext context) {
    removeLoadingOverlay();
    _instanceOfEntry = OverlayEntry(
        builder: (context) => Positioned(
            left: 0,
            top: 0,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.height,
                child: Center(
                  child: Container(
                      width: 75,
                      height: 75,
                      child: Card(
                        child: Center(
                            child: Platform.isIOS
                                ? CupertinoActivityIndicator()
                                : CircularProgressIndicator()),
                      )),
                ))));
    Overlay.of(context).insert(_instanceOfEntry);
  }

  static void showContentOverlay(BuildContext context, Widget child) {
    removeLoadingOverlay();
    _instanceOfEntry = OverlayEntry(
        builder: (context) => Positioned(
            left: 0,
            top: 0,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.height,
              child: Center(child: child),
            )));
    Overlay.of(context).insert(_instanceOfEntry);
  }

  static void removeLoadingOverlay() {
    if (_instanceOfEntry != null) {
      _instanceOfEntry.remove();
      _instanceOfEntry = null;
    }
  }
}
