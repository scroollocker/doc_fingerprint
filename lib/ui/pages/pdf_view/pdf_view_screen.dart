import 'dart:async';

import 'package:doc_fingerprint/models/document_model.dart';
import 'package:doc_fingerprint/service/pdf_helper.dart';
import 'package:doc_fingerprint/ui/components/center_circular_loading.dart';
import 'package:doc_fingerprint/ui/components/center_error_message.dart';
import 'package:doc_fingerprint/ui/components/pdf_modal_buttons/sign_confirm_buttons.dart';
import 'package:doc_fingerprint/ui/components/pdf_modal_buttons/sign_start_buttons.dart';
import 'package:doc_fingerprint/ui/pages/pdf_view/pdf_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:path_provider/path_provider.dart';

class PdfViewScreen extends StatefulWidget {
  // final String pdfUrl;
  final PdfViewController controller;
  final DocumentModel documentModel;
  final bool signConfirm;

  PdfViewScreen(
      {@required this.controller,
      @required this.documentModel,
      this.signConfirm = false}) {
    // assert(pdfUrl != null);
  }

  @override
  _PdfViewScreenState createState() => _PdfViewScreenState();
}

class _PdfViewScreenState extends State<PdfViewScreen>
    with TickerProviderStateMixin {
  bool _pdfLoading = true;
  bool _pdfLoaded = false;
  bool _hasError = false;
  bool _isSignBottomSheetShowed = false;

  bool _showBottomArrow = false;

  AnimationController _arrowController;
  Animation<double> _arrowAnimation;

  PdfController _pdfController;

  String _pdfFilePath;

  bool _isBottomSheetShowed() {
    return _isSignBottomSheetShowed;
  }

  void _cloaseBottomSheet() {
    if (_isSignBottomSheetShowed == true) {
      if (!mounted) return;
      Navigator.pop(context);
    }
  }

  Future<void> _loadPdf(String url) async {
    setState(() => _pdfLoading = true);
    try {
      String path = url;
      if (url.startsWith('http')) {
        var pdfName =
            '${DateTime.now().millisecondsSinceEpoch.toString()}_doc_unsigned.pdf';
        var tmpDir = await getTemporaryDirectory();
        String _tmpPdfPath = tmpDir.path + pdfName;

        path = await PdfHelper.downloadPdf(url: url, path: _tmpPdfPath);
      }

      _pdfFilePath = path;

      if (_pdfController == null) {
        _pdfController = PdfController(
            document: PdfDocument.openFile(_pdfFilePath),
            viewportFraction: 1.0);
      } else {
        _pdfController.loadDocument(PdfDocument.openFile(_pdfFilePath));
      }

      _arrowAnimation =
          Tween<double>(begin: 0.0, end: 8.0).animate(_arrowController);

      _arrowController.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _arrowController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _arrowController.forward();
        }
      });
      _arrowController.forward();

      setState(() {
        _pdfLoading = false;
      });
    } catch (err) {
      print('Download error');
      print(err);
      // rethrow;
      setState(() => _hasError = true);
    }
  }

  @override
  void initState() {
    super.initState();
    print('Init');
    _arrowController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
    );

    widget.controller?.loadPdf = _loadPdf;
    widget.controller?.isBottomSheetShowed = _isBottomSheetShowed;
    widget.controller?.closeBottomSheet = _cloaseBottomSheet;
    // _loadPdf(widget.pdfUrl);
  }

  @override
  void dispose() {
    _arrowController?.dispose();
    _pdfController?.dispose();
    // _closeBottomSheets();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        child: SafeArea(
            child: AnimatedSwitcher(
          duration: Duration(milliseconds: 200),
          child: _hasError
              ? CenterErrorMessage(
                  errorText:
                      'Произошла ошибка отображения документа, обратитесь к специалисту')
              : _pdfLoading
                  ? CenterCircularLoading()
                  : Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 32.0 + 80.0),
                          child: PdfView(
                            controller: _pdfController,
                            documentLoader: CenterCircularLoading(),
                            pageSnapping: true,
                            // scale: 2.0,
                            renderer: (PdfPage page) => page.render(
                              width: page.width * 3,
                              height: page.height * 3,
                              format: PdfPageFormat.JPEG,
                              backgroundColor: '#FFFFFF',
                            ),
                            onDocumentError: (_) {
                              print('Document error');
                              setState(() => _hasError = true);
                            },
                            scrollDirection: Axis.vertical,
                            onDocumentLoaded: (_) {
                              setState(() {
                                _pdfLoaded = true;
                                if (_pdfController.pagesCount == 1) {
                                  _showBottomArrow = false;
                                } else {
                                  _showBottomArrow = true;
                                }
                              });
                            },
                            onPageChanged: (page) {
                              print('$page == ${_pdfController.pagesCount}');
                              if (page == _pdfController.pagesCount) {
                                setState(() {
                                  _showBottomArrow = false;
                                });
                              } else {
                                setState(() {
                                  _showBottomArrow = true;
                                });
                              }
                            },
                          ),
                        ),
                        Container(
                          child: _showBottomArrow == true
                              ? AnimatedBuilder(
                                  animation: _arrowAnimation,
                                  builder: (context, _) {
                                    return Positioned(
                                      left: 0,
                                      right: 0,
                                      bottom: _arrowAnimation.value + 32 + 80,
                                      child: Center(
                                          child: Container(
                                              child: Icon(
                                                  Icons.keyboard_arrow_down))),
                                    );
                                  })
                              : SizedBox(),
                        ),
                        Positioned(
                            bottom: 32,
                            left: 0,
                            right: 0,
                            child: Container(
                                height: 80,
                                child: _pdfLoaded == false || _hasError == true
                                    ? SizedBox()
                                    : widget.signConfirm == false
                                        ? SignStartButtons(
                                            documentModel: widget.documentModel,
                                            onChangeShowState: (v) =>
                                                _isSignBottomSheetShowed = v,
                                            pdfPath: _pdfFilePath)
                                        : SignConfirmButtons(
                                            documentModel: widget.documentModel,
                                          ))),
                      ],
                    ),
        )),
      ),
    );
  }
}
