class PdfViewController {
  Future<void> Function(String url) loadPdf;
  bool Function() isBottomSheetShowed;
  void Function() closeBottomSheet;
}
