import 'dart:async';

import 'package:doc_fingerprint/bloc/socket/socket_bloc.dart';
import 'package:doc_fingerprint/models/document_data_store.dart';
import 'package:doc_fingerprint/service/settings_helper.dart';
import 'package:doc_fingerprint/ui/components/loading_overlay.dart';
import 'package:doc_fingerprint/ui/pages/main/advert_screen.dart';
import 'package:doc_fingerprint/ui/pages/main/settings_screen.dart';
import 'package:doc_fingerprint/ui/pages/pdf_view/pdf_view_controller.dart';
import 'package:doc_fingerprint/ui/pages/pdf_view/pdf_view_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

enum ConnectionStatus { CONNECTED, RECONNECTING }

class MainScreen extends StatefulWidget {
  final String deviceId;

  MainScreen({this.deviceId});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var pdfPath = '';
  var _deviceId = '';
  // var _connectionStatus = '';
  ConnectionStatus _connectStatus;
  PdfViewController _pdfViewController;
  int _timeOutFromReconnecting = 0;

  @override
  void initState() {
    super.initState();
    _pdfViewController = PdfViewController();
    if (widget.deviceId != null) {
      setState(() {
        _deviceId = widget.deviceId;
        // _connectionStatus = 'Планшет работает корректно';
        _connectStatus = ConnectionStatus.CONNECTED;
      });
      BlocProvider.of<SocketBloc>(context)
          .add(ConnectWithDeviceId(widget.deviceId));
    } else {
      BlocProvider.of<SocketBloc>(context).add(InitSocketEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SocketBloc, SocketState>(
      cubit: BlocProvider.of<SocketBloc>(context),
      listener: (context, state) async {
        if (state is OnSocketConnected) {
          print('On socket connected');

          String deviceId = await GetIt.I<SettingsHelper>().getDeviceId();
          if (deviceId != null) {
            var isTimeOut = (DateTime.now().millisecondsSinceEpoch -
                    _timeOutFromReconnecting) >
                35000;
            if (GetIt.I<DocumentDataStore>().deviceId != deviceId ||
                isTimeOut == true) {
              GetIt.I<DocumentDataStore>().deviceId = deviceId;
              print('Start connect with device id');
              BlocProvider.of<SocketBloc>(context)
                  .add(ConnectWithDeviceId(deviceId));
            }
            setState(() {
              BlocProvider.of<SocketBloc>(context)
                  .add(ReConnectWithDeviceId(deviceId));
              // _connectionStatus = 'Планшет работает корректно';
              _connectStatus = ConnectionStatus.CONNECTED;
              _timeOutFromReconnecting = 0;
            });
          } else {
            if (deviceId == null) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => SettingsScreen()),
                  (route) => false);
            }
          }
        }
        if (state is OnInitComplete) {
          BlocProvider.of<SocketBloc>(context).add(LoadDeviceIdFromSettings());
        }
        if (state is OnDeviceIdLoaded) {
          setState(() {
            _deviceId = state.deviceId;
          });
        }
        if (state is OnLoadingScreen) {
          print('Loading ...');
        }
        if (state is OnWaitDocument) {
          print('Wait document ...');
          LoadingOverlay.removeLoadingOverlay();
          GetIt.I<DocumentDataStore>().document = null;
          GetIt.I<DocumentDataStore>().signedDocument = null;

          if (_pdfViewController?.closeBottomSheet != null) {
            print('Start close bottomsheet');

            _pdfViewController.closeBottomSheet();
            // _pdfViewController.isBottomSheetShowed = null;
            // _pdfViewController.closeBottomSheet = null;
          }
        }
        if (state is OnIdChange) {
          GetIt.I<DocumentDataStore>().deviceId = state.deviceId;
          print('Start connect with device id after change');
          print('Device id = ${state.deviceId}');
          BlocProvider.of<SocketBloc>(context)
              .add(ConnectWithDeviceId(state.deviceId));
        }
        if (state is OnDocumentLoaded) {
          print(state.document);
          GetIt.I<DocumentDataStore>().document = state.document;
          Timer(Duration(milliseconds: 500), () {
            // _pdfViewController.loadPdf(state.document.path);
            _pdfViewController.loadPdf(state.document.path);
          });
        }
        if (state is OnDocumentSignConfirmed) {
          Timer(Duration(milliseconds: 500), () {
            // _pdfViewController.loadPdf(state.document.path);
            _pdfViewController.loadPdf(state.document.path);
          });
          GetIt.I<DocumentDataStore>().signedDocument = state.document;
        }

        if (state is ReconnectingState) {
          setState(() {
            // _connectionStatus = 'Идет переподключение к оператору ...';
            _connectStatus = ConnectionStatus.RECONNECTING;
            _timeOutFromReconnecting = DateTime.now().millisecondsSinceEpoch;
          });
        }
        if (state is UploadingState) {
          LoadingOverlay.showLoadingOverlay(context);
        }
      },
      child: Scaffold(
          body: BlocBuilder<SocketBloc, SocketState>(
        buildWhen: (prev, state) {
          var r = state is OnSocketConnected == false &&
              state is OnIdChange == false &&
              state is ReconnectingState == false &&
              state is UploadingState == false &&
              state is OnDeviceIdLoaded == false;
          return r;
        },
        builder: (context, state) {
          Widget bodyWidget = Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Center(child: ,),
                Center(child: CircularProgressIndicator())
              ],
            ),
          );

          if (state is OnWaitDocument) {
            bodyWidget = AdvertScreen();
          }

          if (state is OnDocumentLoaded) {
            print('reloaded pdf');
            bodyWidget = Container(
              key: Key('pdf-loaded-pdf-screen'),
              child: PdfViewScreen(
                // pdfUrl: pdfPath,
                documentModel: state.document,
                controller: _pdfViewController,
                signConfirm: false,
              ),
            );
          }

          if (state is OnDocumentSignConfirmed) {
            print('reloaded pdf');
            bodyWidget = Container(
              key: Key('pdf-confirm-pdf-screen'),
              child: PdfViewScreen(
                // pdfUrl: pdfPath,
                documentModel: state.document,
                controller: _pdfViewController,
                signConfirm: true,
              ),
            );
          }

          return SafeArea(
            child: Column(
              children: [
                Container(
                  // height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.black12))),
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(_deviceId != null
                                ? 'Устройство №$_deviceId'
                                : '')),
                        //Text('$_connectionStatus')
                        _connectStatus != null
                            ? Container(
                                margin: EdgeInsets.only(right: 20),
                                decoration: BoxDecoration(
                                    color: _connectStatus ==
                                            ConnectionStatus.CONNECTED
                                        ? Colors.green
                                        : Colors.yellow,
                                    borderRadius: BorderRadius.circular(100)),
                                width: 10,
                                height: 10,
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
                Expanded(child: bodyWidget),
              ],
            ),
          );
        },
      )),
    );
  }
}
