import 'package:doc_fingerprint/bloc/socket/socket_bloc.dart';
import 'package:doc_fingerprint/models/document_data_store.dart';
import 'package:doc_fingerprint/ui/pages/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  var _formKey = GlobalKey<FormState>();
  var _deviceIdController = TextEditingController();
  var _isLoading = false;
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  void _setSettings() {
    if (_formKey.currentState.validate()) {
      BlocProvider.of<SocketBloc>(context)
          .add(ChangeDeviceId(_deviceIdController.text));
    }
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<SocketBloc>(context).add(InitSocketEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SocketBloc, SocketState>(
      listener: (context, state) {
        if (state is LoadingState) {
          setState(() {
            _isLoading = true;
          });
        }
        if (state is OnSocketError) {
          setState(() {
            _isLoading = false;
          });
          _scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text('${state.error}')));
        }
        if (state is OnIdChange) {
          GetIt.I<DocumentDataStore>().deviceId = state.deviceId;
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(deviceId: state.deviceId)),
              (_) => false);
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          alignment: Alignment.center,
          child: Material(
            elevation: 2,
            child: Container(
              padding: EdgeInsets.all(16),
              color: Colors.white,
              width: 500,
              height: 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'Настройки приложения',
                            style: TextStyle(fontSize: 24),
                          ),
                        ),
                        Divider(),
                        ListView(
                          shrinkWrap: true,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 35),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Идентификатор устройства',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    TextFormField(
                                      controller: _deviceIdController,
                                      maxLength: 5,
                                      validator: (val) {
                                        if (val == null ||
                                            val?.isEmpty == true) {
                                          return 'Значение поля не должно быть пустым';
                                        }
                                        try {
                                          int.parse(val);
                                        } catch (_) {
                                          return 'Можно вводить только целые числа';
                                        }
                                        return null;
                                      },
                                      keyboardType:
                                          TextInputType.numberWithOptions(),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      AnimatedSwitcher(
                        // transitionBuilder: (child, animation) =>
                        //     ScaleTransition(scale: animation),
                        duration: Duration(milliseconds: 300),
                        child: _isLoading == true
                            ? CircularProgressIndicator()
                            : RaisedButton(
                                color: Colors.blue,
                                onPressed: _setSettings,
                                child: Text(
                                  'Отправить'.toUpperCase(),
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                      ),
                    ],
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
