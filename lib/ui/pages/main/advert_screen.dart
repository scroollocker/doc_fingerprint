import 'package:doc_fingerprint/constants/settings_constants.dart';
import 'package:doc_fingerprint/models/document_data_store.dart';
import 'package:doc_fingerprint/ui/components/loading_overlay.dart';
import 'package:doc_fingerprint/ui/components/ui_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get_it/get_it.dart';

class AdvertScreen extends StatefulWidget {
  @override
  _AdvertScreenState createState() => _AdvertScreenState();
}

class _AdvertScreenState extends State<AdvertScreen> {
  bool _isError = false;
  FlutterWebviewPlugin _flutterWebviewPlugin;
  @override
  void initState() {
    super.initState();
    _flutterWebviewPlugin = new FlutterWebviewPlugin();

    _flutterWebviewPlugin.onHttpError.listen((event) async {
      _isError = true;
      await _flutterWebviewPlugin.hide();
      print('Status = ${event.code}');
      _showError();
    });

    _flutterWebviewPlugin.onStateChanged.listen((event) {
      print('Loaded status = ${event.type}');
      if (event.type == WebViewState.finishLoad) {
        if (!_isError) {
          _flutterWebviewPlugin.show();
          LoadingOverlay.removeLoadingOverlay();
        }
      }
    });
  }

  void _showError() {
    LoadingOverlay.showContentOverlay(
        context,
        Column(
          children: [
            Expanded(
              child: Center(
                child: Image.asset('assets/imgs/no_internet.png'),
              ),
            ),
            Center(
                child: Padding(
              padding: const EdgeInsets.only(bottom: 120.0),
              child: UIButton(
                  text: 'Обновить',
                  color: Colors.green,
                  textColor: Colors.white,
                  onTap: () async {
                    _isError = false;
                    await _flutterWebviewPlugin
                        .reloadUrl(SettingsConstants.webViewContentUrl);
                  }),
            ))
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    var devId = GetIt.I<DocumentDataStore>().deviceId;
    print('Advert device id is = ${GetIt.I<DocumentDataStore>().deviceId}');

    return WebviewScaffold(
      url: '${SettingsConstants.webViewContentUrl}?msisdn=$devId',
      appCacheEnabled: true,
      initialChild: Center(
        child: CircularProgressIndicator(),
      ),
      withJavascript: true,
      withLocalStorage: true,
    );
  }
}
