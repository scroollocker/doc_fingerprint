class SettingsConstants {
  static const String settingsBoxName = 'device_settings';
  static const String settingsDeviceIdKey = 'device_id_key';
  static const String apiUrlAddress = 'http://10.230.165.49:3003/';
  static const String webViewContentUrl = 'https://tablet.megacom.kg/';
}
