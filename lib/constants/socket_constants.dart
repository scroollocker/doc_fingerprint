class SocketEventConstants {
  static const String onDeviceIdChanged = 'on_device_id_changed';
  static const String onDeviceSetAwait = 'device_set_await';
  static const String onPrepareSignDoc = 'prepare_sign_doc';
}

class SocketActionConstants {
  static const String deviceConnected = 'device_connected';
  static const String signDocInProgress = 'sign_doc_in_process';
  static const String finishSignDoc = 'finish_sign_doc';
  static const String signCancel = 'sign_cancel';
  static const String deviceIdChanged = 'device_id_changed';
}
