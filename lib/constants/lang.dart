class Lang {
  static final String sign_document_btn = 'sign_document_btn';
  static final String cancel_sign_doc = 'cancel_sign_doc';
  static final String sign_confirm = 'sign_confirm';
  static final String sign_clear = 'sign_clear';
  static final String sign_cancel = 'sign_cancel';
  static final String confirm_sign = 'confirm_sign';
  static final String resign_btd = 'resign_btn';

  static Map<String, String> _ruLocaleData = {
    'sign_document_btn': 'Подписать',
    'cancel_sign_doc': 'Отказаться',
    'sign_confirm': 'OK',
    'sign_clear': 'Сброс',
    'sign_cancel': 'Отмена',
    'confirm_sign': 'Подтвердить подпись',
    'resign_btn': 'Назад'
  };

  static Map<String, String> _kgLocaleData = {
    'sign_document_btn': 'Кол тамга коюу',
    'cancel_sign_doc': 'Баш тартуу',
    'sign_confirm': 'OK',
    'sign_clear': 'Кайрадан баштоо',
    'sign_cancel': 'Жокко чыгаруу',
    'confirm_sign': 'Кол тамганы тастыктоо',
    'resign_btn': 'Артка'
  };

  static Map<String, String> _enLocaleData = {
    'sign_document_btn': 'Sign',
    'cancel_sign_doc': 'Refuse',
    'sign_confirm': 'OK',
    'sign_clear': 'Reset',
    'sign_cancel': 'Cancel',
    'confirm_sign': 'Confirm signature',
    'resign_btn': 'Back'
  };

  static String tr(String lang, String code) {
    switch (lang) {
      case 'ru':
        return _ruLocaleData[code];
      case 'kg':
        return _kgLocaleData[code];
      case 'en':
        return _enLocaleData[code];
      default:
        return '';
    }
  }
}
