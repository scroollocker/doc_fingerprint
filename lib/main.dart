import 'dart:async';

import 'package:doc_fingerprint/bloc/simple_bloc_observer.dart';
import 'package:doc_fingerprint/bloc/socket/socket_bloc.dart';
import 'package:doc_fingerprint/constants/settings_constants.dart';
import 'package:doc_fingerprint/models/document_data_store.dart';
import 'package:doc_fingerprint/service/settings_helper.dart';
import 'package:doc_fingerprint/ui/pages/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  // Init Hive db
  getApplicationSupportDirectory().then((directory) {
    Hive.init(directory.path);
    // Init GetIt singltons
    GetIt.I.registerSingleton<SettingsHelper>(SettingsHelper());
    GetIt.I.registerSingleton<DocumentDataStore>(DocumentDataStore());
    Bloc.observer = SimpleBlocObserver();
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    Timer(Duration(milliseconds: 100), () {
      // Connect to socket
      IO.Socket _socket = IO.io(SettingsConstants.apiUrlAddress,
          OptionBuilder().setTransports(['websocket']).build());

      runApp(
        MyApp(_socket),
      );
    });
  });
}

class MyApp extends StatelessWidget {
  final IO.Socket _socket;

  const MyApp(this._socket, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => SocketBloc(_socket),
        child: MaterialApp(
          title: 'Sign App',
          theme: ThemeData(
            scaffoldBackgroundColor: Colors.white,
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: MainScreen(),
        ));
  }
}
